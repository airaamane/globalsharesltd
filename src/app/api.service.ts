import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http'
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';



@Injectable({
  providedIn: 'root'
})
export class APIService {

  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',      
    })
  };
 
  constructor(private http: HttpClient) { }  

  getData(){
    const dataURI = 'http://developerexam.equityplansdemo.com/test/sampledata';
    return this.http.get(dataURI);    
  }

  getCurrentSharePrice(){
    const fmvURI = 'http://developerexam.equityplansdemo.com/test/fmv';
    return this.http.get(fmvURI);
  }
  
  postData (shares: Object): Observable<Object> {
      const postURI = 'https://webhook.site/0f6cfa37-bd0e-4c4d-8450-02ebb999ff8a';
      return this.http.post<Object>(postURI, shares, this.httpOptions);
    }

   
  

}
