import {  Component,  OnInit} from '@angular/core';
import {  APIService} from './api.service';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'Global Shares Mini Project';
  data: any;
  priceObj: number = 0;
  _share_sum_available: number = 0;
  _share_sum_to_sell: number = 0;
  _value_of_shares : number = 0;


  constructor(private api: APIService) {}

  /*calculate value of shares to be sold per certificate*/
  sharesValue(certificate){    
    if(typeof certificate.sharesToSell === 'undefined'){return 0.000;}
    else{
      return certificate.sharesToSell * this.priceObj;
    }    
  }

   /*code run on component initialisation
   * -- fetch data from APIs
   */
  ngOnInit() {
    let observableData = this.api.getData();
    let obsSharePrice = this.api.getCurrentSharePrice();

    obsSharePrice.subscribe((priceObject)=>{
      this.priceObj = priceObject.value;
    })

    observableData.subscribe((response) => {
      let dataPlaceholder: Array<Object>;
      this.data = [...<Array<Object>>response];
      
      for(let cert of this.data){
        this._share_sum_available+=cert.numberOfShares;
        cert.issuedDate = new Date(parseInt(cert.issuedDate.substring(6,19)));
        cert.sharesToSell =  +0;
        cert.sharesValue =  +0;
        
      }
    });

    setInterval(()=>{
      this._value_of_shares = 0;
      obsSharePrice.subscribe((res) => {
         /** needed as workaround for TypeScript strict typing (res does not contain value warning)) */
        let resHolder : any = res;
        this.priceObj =  resHolder.value;        
        for(let cert of this.data){
          this._value_of_shares += this.sharesValue(cert);
        }
       })
    },30000);
 
  }

 /** post shares sold to webhook API   */
  postShares(){
    if(this._share_sum_to_sell >= this._share_sum_available/2){
      if(confirm('you are about to sell more than half your shares, are you sure?')){
        this.sellShares();
      }
    }else{
      this.sellShares();
    }
  }
/**@handles change in shares to be sold input
 * --performs necessary validations
 */
  handleChange(certificate){
    const control = new FormControl(certificate.sharesToSell, [Validators.max(certificate.numberOfShares), Validators.min(0)]);   
    if(control.errors){
      console.log(control.errors);
      alert('You can only sell less than or equal to the number of shares available for this certificate, and must be a positive number!');
      certificate.sharesToSell = 0;
      for(let cert of this.data){
        this._share_sum_to_sell+=cert.sharesToSell;
        cert.sharesValue = this.sharesValue(cert);
        this._value_of_shares += cert.sharesValue;
      }
    }else{
      this._share_sum_to_sell = 0;
      this._value_of_shares = 0;
      for(let cert of this.data){
        this._share_sum_to_sell+=cert.sharesToSell;
        cert.sharesValue = this.sharesValue(cert);
        this._value_of_shares += cert.sharesValue;
      }
    }
    
  }

  sellShares(){
    let sharesToPost = {
      share_price : this.priceObj,
      number_of_shares_sold : this._share_sum_to_sell,
      value_of_shares_sold : this._value_of_shares,
      date_sold : new Date()

    }
  
    let obsPostData = this.api.postData(sharesToPost);
    obsPostData.subscribe((res)=>{
      console.log(`${sharesToPost.share_price} shares sold!`, res);
    })
  }


  cancelAll(){
    this._share_sum_to_sell= 0;
    this._value_of_shares = 0;
    for(let cert of this.data){     
      cert.sharesToSell=0;
      cert.sharesValue = 0;
      
    }
  }


}
